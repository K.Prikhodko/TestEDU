import express from 'express';
import Debug from 'debug';

const auth = express.Router();
const debug = Debug('server:auth');

auth.post('/', (req) => {
  const { email, password, instance } = req.body;
  debug(email, password, instance);
});

export default auth;
