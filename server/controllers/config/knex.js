import pg from 'pg';
import knex from 'knex';
import heroku from './heroku';

pg.defaults.ssl = true;

const knexConfig = {
  client: 'pg',
  connection: {
    user: heroku.user,
    password: heroku.password,
    host: heroku.host,
    port: heroku.port,
    database: heroku.database,
    ssl: true,
    max: 100,
  },
  pool: {
    min: 2,
    max: 10,
  },
};

export default knex(knexConfig);
