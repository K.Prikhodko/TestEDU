/* eslint-disable no-new */

import Vue from 'vue';

// Vue Material
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/engine.scss';

// Sync vue-router with vuex
import { sync } from 'vuex-router-sync';

// Multi-language plugin
import MultiLanguage from 'vue-multilanguage';
import lngPack from './Language';

import App from './Pages/App/App.vue';
import router from './Router';

import store from './Store';

// Basic Style
import './BasicStyle/basic.scss';

Vue.use(MultiLanguage, lngPack);

Vue.use(VueMaterial);

sync(store, router);

new Vue({
  el: '#app',
  components: { App },
  router,
  store,
  template: '<App />',
});
