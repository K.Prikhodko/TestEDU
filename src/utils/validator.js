import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';
import store from '../Store';

export default (form) => {
  const error = {};

  if (!Validator.isEmail(form.email) || !form.email) {
    error.email = true;
    store.dispatch('emailEmpty', true);
  } else {
    delete error.email;
    store.dispatch('emailEmpty', false);
  }

  if (!form.password) {
    error.password = true;
    store.dispatch('passwordEmpty', true);
  } else {
    delete error.password;
    store.dispatch('passwordEmpty', false);
  }

  return {
    error,
    isValid: isEmpty(error),
  };
};

