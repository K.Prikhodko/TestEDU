import actions from './actions';
import mutations from './mutations';
import getters from './getters';

const state = {
  empty: {
    email: false,
  },
};

const store = {
  state,
  actions,
  mutations,
  getters,
};

export default store;
