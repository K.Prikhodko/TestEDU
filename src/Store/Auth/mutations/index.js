/* eslint-disable no-param-reassign */

import * as type from '../types';

const mutations = {
  [type.LOG_IN](state, loginAs) {
    state.access = true;
    state.loginAs = loginAs;
  },
  [type.EMAIL_EMPTY](state, valid) {
    state.empty.email = valid;
  },
  [type.PASSWORD_EMPTY](state, valid) {
    state.empty.password = valid;
  },
};

export default mutations;
