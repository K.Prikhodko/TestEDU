/* eslint-disable no-console */

import axios from 'axios';
import * as type from '../types';

const actions = {
  logIn({ commit }, {
    email,
    password,
    instance,
    remember,
  }) {
    if (remember) {
      console.log(email, password, remember, instance);
      axios.post('/api/auth', {
        email,
        password,
        instance,
      })
        .then((res) => {
          console.log(res.data);
          commit(type.LOG_IN);
        })
        .catch(err => console.error(err));
    } else {
      console.log(email, password, remember, instance);
      axios.post('/api/auth', {
        email,
        password,
        instance,
      })
        .then((res) => {
          console.log(res.data);
          commit(type.LOG_IN, instance);
        })
        .catch(err => console.error(err));
    }
  },
  emailEmpty({ commit }, valid) {
    commit(type.EMAIL_EMPTY, valid);
  },
  passwordEmpty({ commit }, valid) {
    commit(type.PASSWORD_EMPTY, valid);
  },
};

export default actions;
