/* eslint-disable no-param-reassign */

import * as type from '../types';

const mutations = {
  [type.TOGGLE_PANEL](state) {
    state.panel = !state.panel;
  },
};

export default mutations;
