import * as type from '../types';

const actions = {
  togglePanel({ commit }) {
    commit(type.TOGGLE_PANEL);
  },
};

export default actions;
