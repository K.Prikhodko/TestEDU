import Vue from 'vue';
import Vuex from 'vuex';

import user from './User';
import basic from './Basic';
import auth from './Auth';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    user,
    basic,
    auth,
  },
});

export default store;
