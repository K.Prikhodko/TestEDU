import actions from './actions';
import mutations from './mutations';
import getters from './getters';

const state = {
  data: null,
};

const store = {
  state,
  actions,
  mutations,
  getters,
};

export default store;
