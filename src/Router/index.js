import Vue from 'vue';
import Router from 'vue-router';
import Home from '../Pages/Home/Home.vue';
import NotFound from '../Pages/NotFound/NotFound.vue';
import AuthForm from '../Components/AuthForm/AuthForm.vue';
import SignUp from '../Components/SignUp/SignUp.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      children: [
        {
          path: 'signInTeacher',
          component: AuthForm,
          props: {
            signInAs: 'teacher',
          },
        },
        {
          path: 'signInStudent',
          component: AuthForm,
          props: {
            signInAs: 'student',
          },
        },
        {
          path: 'signUp',
          component: SignUp,
        },
      ],
    },
    {
      path: '/notFound',
      name: 'notFound',
      component: NotFound,
    },
    {
      path: '*',
      redirect: '/notFound',
    },
  ],
});
